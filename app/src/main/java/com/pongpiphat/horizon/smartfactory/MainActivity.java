package com.pongpiphat.horizon.smartfactory;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import io.netpie.microgear.Microgear;
import io.netpie.microgear.MicrogearEventListener;

public class MainActivity extends AppCompatActivity {
    public Microgear microgear = new Microgear(this);
    private String appid = "SmartFactoryPilot"; //APP_ID
    private String key = "t6AgTfL1PHIhf0h"; //KEY
    private String secret = "Daq2JADYHcU0dZYEd01YWDHg1"; //SECRET
    private String alias = "androidAppHost";

    private String[] Environment_Data = null;
    private String[] Temp = null;
    private String[] Humid = null;
    private String[] Power_Data = null;
    private String[] Watt = null;

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Bundle bundle = msg.getData();
            String string = bundle.getString("myKey");
//            TextView myTextView =
//                    (TextView)findViewById(R.id.textView);
            //myTextView.append(string+"\n");
        }
    };
    private GaugeCustomViewGroup Temp_viewGroup;
    private GaugeCustomViewGroup Humid_viewGroup;
    private GaugeCustomViewGroup Watt_viewGroup;
    private GaugeCustomViewGroup Amp_viewGroup;
    private TextDescriptionCustomViewGroup TextCustomViewGroup;
    private SwitchToggleCustomViewGroup switchToggleCustomViewGroup1;
    private SwitchToggleCustomViewGroup switchToggleCustomViewGroup2;
    private SwitchToggleCustomViewGroup switchToggleCustomViewGroup3;
    private SwitchToggleCustomViewGroup switchToggleCustomViewGroup4;

    private boolean ck_switch1 =false;
    private boolean ck_switch2 =false;
    private boolean ck_switch3 =false;
    private boolean ck_switch4 =false;

    private boolean Re_ck_switch1 =false;
    private boolean Re_ck_switch2 =false;
    private boolean Re_ck_switch3 =false;
    private boolean Re_ck_switch4 =false;


    private boolean Start_resume_State = true;
    private boolean ConnectNetpie =false;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);





        MicrogearCallBack callback = new MicrogearCallBack();
        microgear.connect(appid,key,secret,alias);
        microgear.setCallback(callback);
        microgear.subscribe("/Factory/Sensor/Environment");
        microgear.subscribe("/Factory/Sensor/Power");
        microgear.subscribe("/Factory/SW/#");
        microgear.subscribe("/Blower");



        Temp_viewGroup = (GaugeCustomViewGroup) findViewById(R.id.TempGauge);
        Humid_viewGroup = (GaugeCustomViewGroup) findViewById(R.id.HumidGauge);
        Watt_viewGroup = (GaugeCustomViewGroup) findViewById(R.id.WattGauge);
        Amp_viewGroup = (GaugeCustomViewGroup) findViewById(R.id.ampGauge);

        TextCustomViewGroup = (TextDescriptionCustomViewGroup) findViewById(R.id.TextViewGroupAir);

        switchToggleCustomViewGroup1 = (SwitchToggleCustomViewGroup) findViewById(R.id.CustomSwitch1);
        switchToggleCustomViewGroup2 = (SwitchToggleCustomViewGroup) findViewById(R.id.CustomSwitch2);
        switchToggleCustomViewGroup3 = (SwitchToggleCustomViewGroup) findViewById(R.id.CustomSwitch3);
        switchToggleCustomViewGroup4 = (SwitchToggleCustomViewGroup) findViewById(R.id.CustomSwitch4);

        switchToggleCustomViewGroup1.setText_Detail("PCB ROOM");
        switchToggleCustomViewGroup2.setText_Detail("3D PRINTER ROOM");
        switchToggleCustomViewGroup3.setText_Detail("FUME HOOD EXHAUST FAN");
        switchToggleCustomViewGroup4.setText_Detail("MACHINE");
        (new Thread(new Runnable()
        {
            int count = 1;
            @Override
            public void run() {
                while (!Thread.interrupted())
                    try {
                        runOnUiThread(new Runnable() // start actions in UI thread
                        {
                            @Override
                            public void run() {
                                //Log.i("Test","Test Thread");
                                //StatusSW1 = switchToggleCustomViewGroup1.isStatusSwitch();
                                int i = switchToggleCustomViewGroup1.getStatusSW_Num();
                                if(switchToggleCustomViewGroup1.getStatusSW_Num() == 1 && ck_switch1 == false){
//                                if(StatusSW1 == true && ck_switch1 == false){
                                    microgear.publish("/Factory/SW/Chanel1","1");
                                    ck_switch1 = true;
                                    Log.i("Message","Publish SW1 false");
                                }
                                if(switchToggleCustomViewGroup1.getStatusSW_Num() == 0 && ck_switch1 == true){
                                    microgear.publish("/Factory/SW/Chanel1","0");
                                    ck_switch1 = false;
                                    Log.i("Message","Publish SW1 true");
                                }
                                if(switchToggleCustomViewGroup2.getStatusSW_Num() == 1 && ck_switch2 == false){
                                    microgear.publish("/Factory/SW/Chanel2","1");
                                    ck_switch2 = true;
                                    Log.i("Message","Publish SW2 false");

                                }
                                if(switchToggleCustomViewGroup2.getStatusSW_Num() == 0 && ck_switch2 == true){
                                    microgear.publish("/Factory/SW/Chanel2","0");
                                    ck_switch2 = false;
                                    Log.i("Message","Publish SW2 true");
                                }
                                if(switchToggleCustomViewGroup3.getStatusSW_Num() == 1 && ck_switch3 == false){
                                    microgear.publish("/Factory/SW/Chanel3","1");
                                    ck_switch3 = true;
                                    Log.i("Message","Publish SW3 false");
                                }
                                if(switchToggleCustomViewGroup3.getStatusSW_Num() == 0 && ck_switch3 == true){
                                    microgear.publish("/Factory/SW/Chanel3","0");
                                    ck_switch3 = false;
                                    Log.i("Message","Publish SW3 true");
                                }
                                if(switchToggleCustomViewGroup4.getStatusSW_Num() == 1 && ck_switch4 == false){
                                    microgear.publish("/Factory/SW/Chanel4","1");
                                    ck_switch4 = true;
                                    Log.i("Message","Publish SW4 false");
                                }
                                if(switchToggleCustomViewGroup4.getStatusSW_Num() == 0 && ck_switch4 == true){
                                    microgear.publish("/Factory/SW/Chanel4","0");
                                    ck_switch4 = false;
                                    Log.i("Message","Publish SW4 true");
                                }

                                if(Re_ck_switch1&&!switchToggleCustomViewGroup1.isPush_Switch()){
                                    switchToggleCustomViewGroup1.setmChangeSb(true);
                                }
                                if(!Re_ck_switch1&&!switchToggleCustomViewGroup1.isPush_Switch()) {
                                    switchToggleCustomViewGroup1.setmChangeSb(false);
                                }

                                if(Re_ck_switch2&&!switchToggleCustomViewGroup2.isPush_Switch()){
                                    switchToggleCustomViewGroup2.setmChangeSb(true);
                                }
                                if(!Re_ck_switch2&&!switchToggleCustomViewGroup2.isPush_Switch()){
                                    switchToggleCustomViewGroup2.setmChangeSb(false);
                                }
                                if(Re_ck_switch3&&!switchToggleCustomViewGroup3.isPush_Switch()){
                                    switchToggleCustomViewGroup3.setmChangeSb(true);
                                }
                                if(!Re_ck_switch3&&!switchToggleCustomViewGroup3.isPush_Switch()){
                                    switchToggleCustomViewGroup3.setmChangeSb(false);
                                }
                                if(Re_ck_switch4&&!switchToggleCustomViewGroup4.isPush_Switch()){
                                    switchToggleCustomViewGroup4.setmChangeSb(true);
                                }
                                if(!Re_ck_switch4&&!switchToggleCustomViewGroup4.isPush_Switch()){
                                    switchToggleCustomViewGroup4.setmChangeSb(false);
                                }
                            }
                        });
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        // ooops
                    }
            }

    })).start();



        (new Thread(new Runnable()
        {
            int count = 1;
            @Override
            public void run()
            {
                while (!Thread.interrupted())
                    try
                    {
                        runOnUiThread(new Runnable() // start actions in UI thread
                        {
                            @Override
                            public void run(){
//                                microgear.publish("Topictest", String.valueOf(count)+".  Test message");
//                                microgear.publish("/Factory/SW/Chanel1","1");
//                                count++;
                                    if(Environment_Data != null){

                                        Temp_viewGroup.setGaugeView(Math.round(Float.parseFloat(getEnvironment()[1])));
                                        Temp_viewGroup.setValueText(getEnvironment()[1]);
                                        Temp_viewGroup.setDesText("Temp");
                                        Temp_viewGroup.setUnitText("°C");


                                        Humid_viewGroup.setGaugeView(Math.round(Float.parseFloat(getEnvironment()[0])));
                                        Humid_viewGroup.setValueText(getEnvironment()[0]);
                                        Humid_viewGroup.setDesText("Humid");
                                        Humid_viewGroup.setUnitText("%");


                                        TextCustomViewGroup.setTextViewTitle("AIR QUALITY");
                                        if(getEnvironment()[2].equals("0.00")){
                                            TextCustomViewGroup.setTextViewDes("High pollution! Force signal active");
                                        }
                                        if(getEnvironment()[2].equals("1.00")){
                                            TextCustomViewGroup.setTextViewDes("High pollution!");
                                        }
                                        if(getEnvironment()[2].equals("2.00")){
                                            TextCustomViewGroup.setTextViewDes("High pollution!");
                                        }
                                        if(getEnvironment()[2].equals("3.00")){
                                            TextCustomViewGroup.setTextViewDes("Fresh air");
                                        }



                                    }
                                    if(Power_Data != null){

                                        Amp_viewGroup.setGaugeView(Math.round(Float.parseFloat(getPower()[0])));
                                        Amp_viewGroup.setValueText(getPower()[0]);
                                        Amp_viewGroup.setDesText("Amp");
                                        Amp_viewGroup.setUnitText("A");


                                        Watt_viewGroup.setGaugeView(Math.round(Float.parseFloat(getPower()[1])));
                                        Watt_viewGroup.setValueText(getPower()[1]);
                                        Watt_viewGroup.setDesText("Watt");
                                        Watt_viewGroup.setUnitText("W");
                                    }

                                if(Start_resume_State&& ConnectNetpie){
                                    if(count==1){
                                        microgear.publish("/Factory/SW/Chanel1","?");
                                    }
                                    if(count==2){
                                        microgear.publish("/Factory/SW/Chanel2","?");
                                    }
                                    if(count==3){
                                        microgear.publish("/Factory/SW/Chanel3","?");
                                    }
                                    if(count==4){
                                        microgear.publish("/Factory/SW/Chanel4","?");
                                    }
                                    count = 1;
                                    Start_resume_State =false;
                                }
//
                            }
                        });
                        Thread.sleep(2000);
                    }
                    catch (InterruptedException e)
                    {
                        // ooops
                    }
            }
        })).start();

    }


    protected void onDestroy() {
        super.onDestroy();
        microgear.disconnect();
    }

    protected void onResume() {
        super.onResume();
        microgear.bindServiceResume();
        Start_resume_State = true;
    }

    class MicrogearCallBack implements MicrogearEventListener {

        @Override
        public void onConnect() {
            Message msg = handler.obtainMessage();
            Bundle bundle = new Bundle();
            bundle.putString("myKey", "Now I'm connected with netpie");
            msg.setData(bundle);
            handler.sendMessage(msg);
            Log.i("Connected","Now I'm connected with netpie");
            ConnectNetpie =true;


        }

        @Override
        public void onMessage(String topic, String message) {
            Message msg = handler.obtainMessage();
            if(topic.equals("/SmartFactoryPilot/Factory/Sensor/Environment")){
                setEnvironment(message.split("/"));
                Temp = Environment_Data[0].split(",");
                Humid = Environment_Data[1].split(",");

                //Log.i("Message",topic+" : "+message);
            }
            if(topic.equals("/SmartFactoryPilot/Factory/Sensor/Power")){

                setPower(message.split("/"));
                Watt = Power_Data[0].split(",");

                //Log.i("Message",topic+" : "+message);
            }
            if(topic.equals("/SmartFactoryPilot/Factory/SW/Chanel1")){
                if (message.equals("?")){
                    Log.i("Message","Text ReCheck");
                }
                if(message.equals("1")){

                    Re_ck_switch1 = false;
                    Log.i("Message","Text ReCheck SW1 false");
                }
                if(message.equals("0")){
                    Re_ck_switch1 = true;
                    Log.i("Message","Text ReCheck SW1 true");
                }
            }

            if(topic.equals("/SmartFactoryPilot/Factory/SW/Chanel2")){
                if (message.equals("?")){
                    Log.i("Message","Text ReCheck");
                }
                if(message.equals("1")){
                    Log.i("Message","Text ReCheck SW2 false");
                    Re_ck_switch2 = false;
                }
                if(message.equals("0")){
                    Log.i("Message","Text ReCheck SW2 true");
                    Re_ck_switch2 = true;
                }
            }if(topic.equals("/SmartFactoryPilot/Factory/SW/Chanel3")){
                if (message.equals("?")){
                    Log.i("Message","Text ReCheck");
                }
                if(message.equals("1")){
                    Log.i("Message","Text ReCheck SW3 false");
                    Re_ck_switch3 = false;
                }
                if(message.equals("0")){
                    Log.i("Message","Text ReCheck SW3 true");
                    Re_ck_switch3 = true;
                }
            }
            if(topic.equals("/SmartFactoryPilot/Factory/SW/Chanel4")){
                if (message.equals("?")){

                }
                if(message.equals("1")){
                    Log.i("Message","Text ReCheck SW4 false");
                    Re_ck_switch4 = false;
                }
                if(message.equals("0")){
                    Log.i("Message","Text ReCheck SW4 true");
                    Re_ck_switch4 = true;
                }
            }


            Bundle bundle = new Bundle();
            bundle.putString("myKey", topic+" : "+message);
            msg.setData(bundle);
            handler.sendMessage(msg);
            Log.i("Message",topic+" : "+message);
        }

        @Override
        public void onPresent(String token) {
            Message msg = handler.obtainMessage();
            Bundle bundle = new Bundle();
            bundle.putString("myKey", "New friend Connect :"+token);
            msg.setData(bundle);
            handler.sendMessage(msg);
            Log.i("present","New friend Connect :"+token);
        }

        @Override
        public void onAbsent(String token) {
            Message msg = handler.obtainMessage();
            Bundle bundle = new Bundle();
            bundle.putString("myKey", "Friend lost :"+token);
            msg.setData(bundle);
            handler.sendMessage(msg);
            Log.i("absent","Friend lost :"+token);
        }

        @Override
        public void onDisconnect() {
            Message msg = handler.obtainMessage();
            Bundle bundle = new Bundle();
            bundle.putString("myKey", "Disconnected");
            msg.setData(bundle);
            handler.sendMessage(msg);
            Log.i("disconnect","Disconnected");
            ConnectNetpie = false;
        }

        @Override
        public void onError(String error) {
            Message msg = handler.obtainMessage();
            Bundle bundle = new Bundle();
            bundle.putString("myKey", "Exception : "+error);
            msg.setData(bundle);
            handler.sendMessage(msg);
            Log.i("exception","Exception : "+error);
        }

        @Override
        public void onInfo(String info) {
            Message msg = handler.obtainMessage();
            Bundle bundle = new Bundle();
            bundle.putString("myKey", "Exception : "+info);
            msg.setData(bundle);
            handler.sendMessage(msg);
            Log.i("info","Info : "+info);
        }
    }

    public String[] getEnvironment(){
        return this.Environment_Data;
    }

    //public method to set the age variable
    public void setEnvironment(String[] Data){
        this.Environment_Data = Data;
    }
    public String[] getPower(){
        return this.Power_Data;
    }

    //public method to set the age variable
    public void setPower(String[] Data){
        this.Power_Data = Data;
    }

    public void Publish(String Topic,String Meg){
        microgear.publish("Topictest",". Test message");
    }

    public void setMicrogear(Microgear microgear) {
        this.microgear = microgear;
    }

    public Microgear getMicrogear() {
        return microgear;
    }
}
