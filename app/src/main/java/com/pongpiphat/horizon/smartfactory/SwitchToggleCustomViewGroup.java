package com.pongpiphat.horizon.smartfactory;

import android.content.Context;
import android.os.Build;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.annotation.StyleRes;
import android.util.AttributeSet;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kyleduo.switchbutton.SwitchButton;

/**
 * Created by Horizon on 26-Sep-17.
 */

public class SwitchToggleCustomViewGroup extends FrameLayout {
    private TextView Text_Detail;
    private SwitchButton mChangeSb;
    private boolean StatusSwitch = Boolean.parseBoolean(null);
    private boolean Push_Switch = false;


    private int StatusSW_Num = 2;

    public int getStatusSW_Num() {
        return StatusSW_Num;
    }

    public SwitchToggleCustomViewGroup(@NonNull Context context) {
        super(context);
        initInflate();
        initInstances(context);
    }

    public SwitchToggleCustomViewGroup(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initInflate();
        initInstances(context);
    }

    public SwitchToggleCustomViewGroup(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initInflate();
        initInstances(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public SwitchToggleCustomViewGroup(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr, @StyleRes int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initInflate();
        initInstances(context);
    }
    private void initInflate(){
        // Inflate Layout Here
        inflate(getContext(),R.layout.customviewswitchtoggle,this);

    }
    private void initInstances(final Context context){
        //findView by ID
        //final Microgear microgear = new Microgear(context);

        mChangeSb = (SwitchButton) findViewById(R.id.sb_Customview);
        Text_Detail = (TextView) findViewById(R.id.textDetail);
        mChangeSb.setChecked(true);

        //MainActivity main = new MainActivity();
        //microgear.publish("/Factory/SW/Chanel1","1");

        mChangeSb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                Toast.makeText(context, "Switch" + (isChecked ? "  ON" : "  OFF"), Toast.LENGTH_SHORT).show();
//                if(isChecked){
//                    // Send Action to ON Switch
//                    //main.Pubblish("/Factory/SW/Chanel1","1");
//                    //microgear.publish("/Factory/SW/Chanel1","1");
//                    //main.getMicrogear().publish("/Factory/SW/Chanel1","1");
//                    Toast.makeText(context,"Switch ON ", Toast.LENGTH_SHORT).show();
//                    StatusSwitch = false;
//                }else {
//                    //Main.Pubblish("/Factory/SW/Chanel1","0");
//                    Toast.makeText(context,"Switch OFF ", Toast.LENGTH_SHORT).show();
//                    //microgear.publish("/Factory/SW/Chanel1","0");
//                    StatusSwitch = true;
//                }
            }
        });
        mChangeSb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //toast(context,"Toggle SwitchButton new check state: " + (isChecked ? "Checked" : "Unchecked"));
                if(isChecked){
                    // Send Action to ON Switch
                    //main.Pubblish("/Factory/SW/Chanel1","1");
                    //microgear.publish("/Factory/SW/Chanel1","1");
                    //main.getMicrogear().publish("/Factory/SW/Chanel1","1");
                    Toast.makeText(context,"Switch ON ", Toast.LENGTH_SHORT).show();
                    StatusSwitch = false;
                    Push_Switch = true;
                    StatusSW_Num = 0;
                    mChangeSb.setBackColorRes(R.color.md_green_600);
                }else {
                    //Main.Pubblish("/Factory/SW/Chanel1","0");
                    Toast.makeText(context,"Switch OFF ", Toast.LENGTH_SHORT).show();
                    //microgear.publish("/Factory/SW/Chanel1","0");
                    StatusSwitch = true;
                    Push_Switch = true;
                    StatusSW_Num = 1;
                    mChangeSb.setBackColorRes(R.color.md_red_500);
                }
            }
        });

    }

    public boolean isPush_Switch() {
        return Push_Switch;
    }
    public void setPush_Switch(boolean bool){
        Push_Switch = bool;
    }

    private void SetBackColorRes(int R_color){
        mChangeSb.setBackColorRes(R_color);
    }
    private void toast(Context context,String text){
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }
    public TextView getText_Detail() {
        return Text_Detail;
    }

    public SwitchButton getmChangeSb() {
        return mChangeSb;
    }

    public boolean isStatusSwitch() {
        return StatusSwitch;
    }




    public void setText_Detail(String textDetail) {
        Text_Detail.setText(textDetail);
    }


    public void setmChangeSb(boolean bool)
    {
        //mChangeSb.setChecked(bool);

        mChangeSb.setCheckedImmediatelyNoEvent(bool);
        StatusSwitch = bool;
        if (bool == false){
            //setPush_Switch(true);
            if (Push_Switch == true){
                StatusSW_Num = 1;
            }

            SetBackColorRes(R.color.md_red_500);
        }else {
            //setPush_Switch(true);
            if(Push_Switch == true){
                StatusSW_Num = 0;
            }

            SetBackColorRes(R.color.md_green_600);
        }

//        mChangeSb.setClickable(bool);
    }

}
