package com.pongpiphat.horizon.smartfactory;

import android.content.Context;
import android.os.Build;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.annotation.StyleRes;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.TextView;

import pl.pawelkleczkowski.customgauge.CustomGauge;


/**
 * Created by Horizon on 25-Sep-17.
 */

public class GaugeCustomViewGroup extends FrameLayout{
    private CustomGauge gaugeView;
    private TextView ValueText;
    private TextView DesText;
    private TextView UnitText;

    public GaugeCustomViewGroup(@NonNull Context context) {
        super(context);
        initInflate();
        initInstances();
    }

    public GaugeCustomViewGroup(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initInflate();
        initInstances();
    }

    public GaugeCustomViewGroup(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initInflate();
        initInstances();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public GaugeCustomViewGroup(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr, @StyleRes int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initInflate();
        initInstances();
    }
    private void initInflate(){
        // Inflate Layout Here
        inflate(getContext(),R.layout.customviewgauge,this);
    }
    private void initInstances(){
        //findView by ID
        gaugeView = (CustomGauge) findViewById(R.id.CustomView_gauge);
        ValueText = (TextView) findViewById(R.id.CustomView_ValueText);
        DesText = (TextView) findViewById(R.id.CustomView_DisText);
        UnitText = (TextView) findViewById(R.id.CustomView_ValueTextUnit);
        //setGaugeValue(1,5000);
        setAngle(135,270);
        setStrokeWidth(10);
    }
    public void setGaugeView(int t){
        gaugeView.setValue(t);
    }
    public void setGaugeValue(int StartValue,int EndValue){
        gaugeView.setEndValue(EndValue);
        gaugeView.setStartValue(StartValue);
    }
    public void setStrokeWidth(int StrokeWidth){
        gaugeView.setStrokeWidth(StrokeWidth);
    }
    public void setAngle(int StartAngle,int SweepAngle){
        gaugeView.setSweepAngle(SweepAngle);
        gaugeView.setStartAngle(StartAngle);
    }
    public void setColor(int StrokeColor,int StartColor, int EndColor){
        gaugeView.setStrokeColor(StrokeColor);
        gaugeView.setPointStartColor(StartColor);
        gaugeView.setPointEndColor(EndColor);
    }
    public void setValueText(String Value){
        ValueText.setText(Value);
    }
    public void setUnitText(String Value){
        UnitText.setText(Value);
    }
    public void setDesText(String Description){
        DesText.setText(Description);
    }
}
