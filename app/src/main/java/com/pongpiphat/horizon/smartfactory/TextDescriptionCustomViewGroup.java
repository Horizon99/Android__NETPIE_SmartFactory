package com.pongpiphat.horizon.smartfactory;

import android.content.Context;
import android.os.Build;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.annotation.StyleRes;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.TextView;

/**
 * Created by Horizon on 27-Sep-17.
 */

public class TextDescriptionCustomViewGroup extends FrameLayout{
    private TextView textViewTitle,textViewDes;
    public TextDescriptionCustomViewGroup(@NonNull Context context) {
        super(context);
        initInflate();
        initInstances();
    }

    public TextDescriptionCustomViewGroup(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initInflate();
        initInstances();
    }

    public TextDescriptionCustomViewGroup(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initInflate();
        initInstances();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public TextDescriptionCustomViewGroup(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr, @StyleRes int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initInflate();
        initInstances();
    }
    private void initInflate(){
        // Inflate Layout Here
        inflate(getContext(),R.layout.customviewtext,this);
    }
    private void initInstances(){
        //findView by ID
        textViewTitle = (TextView) findViewById(R.id.CustomTextviewTitle);
        textViewDes = (TextView) findViewById(R.id.CustomTextviewDescription);
    }


    public void setTextViewTitle(String textTitle) {
        textViewTitle.setText(textTitle);
    }

    public void setTextViewDes(String textDes) {
        textViewDes.setText(textDes);
    }
}
